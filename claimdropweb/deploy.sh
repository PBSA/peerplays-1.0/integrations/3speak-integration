#!/bin/bash
  
#Checking/Creating the virtual environment
ls | grep "env" > run.log 2>&1
if [ $? == 0 ]
then
  echo "A virtual environment by name env already exists"
else
  echo "Creating virtual environment"
  virtualenv -p python3.8 env
fi

#Activating the environment
echo 'Activating the environment'
source $(pwd)/env/bin/activate

#Installing the requirements
echo 'Installing the requirments' 
pip install -r requirements.txt

#Adding the irona chain id to the chains
echo 'Adding the irona chain id to the config'
sed -i 's/^}/    "IRONA":{\n        "chain_id":"bec1b83fc4752ad319dfc4e9f1fac37d8fb06c77382ad74438a827a4b16f2e9e",\n        "core_symbol":"TEST",\n        "prefix":"TEST",\n    },\n}/' $(pwd)/env/lib/python3.8/site-packages/peerplaysbase/chains.py

#Migrate
python manage.py migrate

echo 'Running the app'
nohup python manage.py runserver $1 > run.log 2>&1 &


