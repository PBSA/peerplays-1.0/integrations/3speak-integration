from django.contrib.auth.models import User
from django import forms
    
class UserForm(forms.ModelForm):
    class Meta:
    	model = User
    	fields = ['username', 'password']
    
class UserRegistrationForm(forms.ModelForm):
    class Meta:
    	model = User
    	fields = [
    		    'username', 
    		    'password',
    	] 
    
    def save(self, commit=True):
        user = super(UserRegistrationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password"])
        if commit:
            user.save()
        return user