#!/usr/bin/env python
# -*- coding: utf-8 -*-

import itertools
from datetime import datetime as dt
import pprint
# from CaseInsensitiveDict import CaseInsensitiveDict
from requests.structures import CaseInsensitiveDict
from beem.nodelist import NodeList
from beem import Hive
import beem
import pymongo
import json
import requests
import pandas as pd
import numpy as np

# head_block_number: 59395501
snapshot_block = 60714039 # Corresponds to 2022-01-07T08:00:00 UTC or 2022-01-07T00:00:00 PST
block = beem.block.Block(snapshot_block)
snapshot_timestamp = block["timestamp"]

wif = "5KQwrPbwdL6PhXujxW37FSSQZ1JiwsST4cqQzDeyXtP79zkvFD3"

urlHive = "https://api.hive.blog"
# urlHive = "http://96.46.48.108:8090"

pp = pprint.PrettyPrinter(indent=4)

def get_hive_nodes():
    nodelist = NodeList()
    nodes = nodelist.get_hive_nodes()
    nodelist.update_nodes(blockchain_instance=Hive(node=nodes, num_retries=10))
    return nodelist.get_hive_nodes()
    #return "https://beta.openhive.network"

bts = Hive(
    node=get_hive_nodes(),
    #node='https://api.hive.blog',
    nobroadcast=True,
    keys={"active": wif},
    num_retires=10
    )

#bts = Hive(
#    node=urlHive,
#    nobroadcast=True,
#    keys={"active": wif},
#    num_retires=10
#    )

def Block(id, bts):
    block = beem.block.Block(id, hive_instance=bts)
    return block.json()

def Blocks(id, bts):
    blocks = beem.block.Blocks(id, 1000, hive_instance=bts)
    return blocks

def DbSetup():
    try:
        # dbClient = pymongo.MongoClient()
        dbClient = pymongo.MongoClient('mongodb://96.46.48.108:27017/',
                                       username="api",
                                       password="This is api password",
                                       authSource="snapshotdb",)
                                       # authMechanism="SCRAM-SHA-256")
    except:
        print("Database server not available on the local machine")
        return False
    # if "snapshotdb" not in dbClient.list_database_names():
    dbSnapshot = dbClient["snapshotdb"]
    # return dbSnapshot
    colBlocks = dbSnapshot["blocks"]
    colAccounts = dbSnapshot["accounts"]
    colClaimStatuses = dbSnapshot["claimStatuses"]
    colSnapHistory = dbSnapshot["snapHistory"]
    colClaimRegister = dbSnapshot["claimRegister"]
    # colClaimStatus = dbSnapshot[""]
    #    print("Collection colBlocks created")
    return colBlocks, colAccounts, colClaimStatuses, colSnapHistory, colClaimRegister
    # else:
    #     return colBlocks

def DbSetupAccounts():
    try:
        dbClient = pymongo.MongoClient()
    except:
        print("Database server not available on the local machine")
        return False
    # if "snapshotdb" not in dbClient.list_database_names():
    dbSnapshot = dbClient["snapshotdb"]
    colAccounts = dbSnapshot["accounts"]
    #    print("Collection colBlocks created")
    return colAccounts


class Snapshot():

    def __init__(self):
        self.bts = bts
#        self.bts = Hive(
#            node=get_hive_nodes(),
#            nobroadcast=True,
#            keys={"active": wif},
#            num_retires=10
#            )
        self.colBlocks, self.colAccounts, self.colClaimStatuses, self.colSnapHistory, self.colClaimRegister = DbSetup()
        # self.colAccounts = DbSetupAccounts()
        pass

    def Transaction(self, id):
        bts = self.bts
        transaction = Block(id, bts)
        return transaction

    def LoopBlocks(self):
        while True:
            self.blocks = Blocks(self.colBlocks.count() + 1, bts)
            self.colBlocks.insert_many(self.blocks)
            blockLast = self.blocks[-1]
            print(blockLast.block_num, self.colBlocks.count(), blockLast["timestamp"])

    def Loop(self, id):
        while True:
            self.id = id
            transaction = self.Transaction(id)
            if len(transaction["transactions"]) != 0:
                return transaction
            else:
                id = id + 1
                print("id:", id)

    def ClaimRegister(self, username):
        data = {}
        data["username"] = username
        data["timestamp"] = dt.now()

    def AccountsGen(self):
        accountsGen = beem.account.Blockchain().get_all_accounts()
        return accountsGen

    def LoopAccounts(self):
        accountsGen = self.AccountsGen()
        next(itertools.islice(accountsGen, 3240, None))
        k = 0
        while True:
            k = k + 1
            username = next(accountsGen)
            account = beem.account.Account(username)
            balances = account.balances
            hive = balances["total"][0]
            amount = hive.amount
            print(k, username, balances["total"])
            if amount > 100:
                self.ToBeClaimedReverseTemp(username)

    def HiveTotal(self):
        cursor = snapshot.colAccounts.find({})
        balanceHiveTotal = 0
        balanceVestTotal = 0
        for c in cursor:
            balanceHive = float(c["balance"]["amount"])
            balanceVest = float(c["reward_vesting_balance"]["amount"]) + \
                float(c["vesting_shares"]["amount"]) + \
                float(c["delegated_vesting_shares"]["amount"]) + \
                float(c["received_vesting_shares"]["amount"])

            balanceHiveTotal = balanceHiveTotal + balanceHive
            balanceVestTotal = balanceVestTotal + balanceVest
            print(c["id"], c["name"], balanceHive, balanceHiveTotal, balanceVest, balanceVestTotal)
        print("Done Balance Hive")
        return balanceHiveTotal, balanceVestTotal

    def HiveUser(self, username):

        # account data from mongodb
        # cursor = self.colAccounts.find({"name":username})
        # c = cursor.next()

        # account data from hive api
        # print("in snapshot.py")
        account = beem.account.Account(username)
        # print("beem account worked snapshot.py")
        print(account)
        c = json.loads(json.dumps(dict(account), default=str))

        balanceHive = float(c["balance"]["amount"])
        print("balance hive=", balanceHive)
        reward_vesting_balance = float(c["reward_vesting_balance"]["amount"])
        vesting_shares = float(c["vesting_shares"]["amount"])
        delegated_vesting_shares = float(c["delegated_vesting_shares"]["amount"])
        received_vesting_shares = float(c["received_vesting_shares"]["amount"])
        print("reward_vesting_balance=", reward_vesting_balance, self.Vest2Hp(reward_vesting_balance))
        print("vesting_shares=", vesting_shares, self.Vest2Hp(vesting_shares))
        print("delegated_vesting_shares=", delegated_vesting_shares, self.Vest2Hp(delegated_vesting_shares))
        print("received_vesting_shares=", received_vesting_shares, self.Vest2Hp(received_vesting_shares))
        balanceVest = float(c["reward_vesting_balance"]["amount"]) + \
            float(c["vesting_shares"]["amount"]) + \
            float(c["delegated_vesting_shares"]["amount"]) + \
            float(c["received_vesting_shares"]["amount"])
        print("total vest, sum of above 4 vests= ", balanceVest)
        balanceHivePower = self.Vest2Hp(vesting_shares)
        print("hivePower=", balanceHivePower)
        # print(username, balanceHive, balanceVest, balanceHivePower)
        # return balanceHive + balanceHivePower # this is for web
        larynx = balanceHive + balanceHivePower
        print("larynx, sum of balanceHive and hivePower =", larynx)
        return larynx, balanceHive, balanceHivePower

    def HiveUserHistorical(self, username):
        account = beem.account.Account(username)
        balances = account.balances
        balances = balances["total"]
        hive = balances[0].amount
        hbd = balances[1].amount
        vests = balances[2].amount
        return hive,hbd, vests

    def History(self, username):
        # history_reverse(start=None, stop=None, use_block_num=True, only_ops=[], exclude_ops=[], batch_size=1000, raw_output=False) method of beem.account.Account instance
        account = beem.account.Account(username)
        historyGen = account.history_reverse(stop=snapshot_block, use_block_num=True)
        # historyGen = account.history(stop=snapshot_block, use_block_num=True)
        # historyGen = account.history(stop=snapshot_block, use_block_num=True)
        # historyGen = account.history()
        return list(historyGen)
        # return historyGen

    def HistoryFromBeginning(self, username):
        # history_reverse(start=None, stop=None, use_block_num=True, only_ops=[], exclude_ops=[], batch_size=1000, raw_output=False) method of beem.account.Account instance
        account = beem.account.Account(username)
        # historyGen = account.history_reverse(stop=snapshot_block, use_block_num=True)
        # historyGen = account.history(stop=snapshot_block, use_block_num=True)
        # historyGen = account.history(stop=None, use_block_num=False)
        historyGen = account.history()
        history = []
        while True:
            histSing = next(historyGen)
            print(histSing["timestamp"])
            history.append(histSing)
            self.history = history
        return history
        # return list(historyGen)

    def BalanceFromHistory(self, history, assetType, nai):
        df = pd.DataFrame(history)
        if assetType in df:
            self.df = df
            # x = df["reward_hive"]
            x = df[assetType]
            y = x.dropna()
            # z = y.values
            # self.z = z
            self.y = y
            bTotal = self.AmountSum(y, nai)
            return bTotal
        else:
            print(assetType, "not in df")
            return 0
    
    def GetOptsFromHistory(self, history, type, action, nai):
        df = pd.DataFrame(history)
        cond = df["type"] == type
        ops = df[cond]
        if not ops.empty:
            return self.AmountSum(ops[action], nai)
        else:
            return 0

    def GetMarketOrders(self, history, type, username, nai):
        df = pd.DataFrame(history)
        cond = df["type"] == type
        ops = df[cond]
        if not ops.empty:
            opsdf = pd.DataFrame(ops)
            # currentPays = opsdf[opsdf["current_owner"] == username]["current_pays"]
            openPays = opsdf[opsdf["open_owner"] != username]["open_pays"]
            return self.AmountSum(openPays, nai)
        else:
            return 0

    def AmountSum(self, z, nai):
        if len(z) == 0:
            return 0
        b = []
        for k in range(len(z)):
            row = z.iloc[k]
            # print("row:", row)
            if row["nai"] == nai:
                precision = row["precision"]
                divider = 10 ** precision
                amountAct = int(row["amount"]) / divider
                b.append(amountAct)
            # b.append(int(z.iloc[k]["amount"]))
        bTotal = np.sum(b)
        self._z = z
        # precision = z.iloc[0]["precision"]
        # divider = 10 ** precision
        # bTotal = bTotal / divider
        return bTotal

    def BalFromTransfers(self, df, username):
        if "type" in df.keys():
            try:
                dft = df.loc[df["type"]=="transfer"]
                try:
                    dftReceived = dft.loc[dft["to"]==username]
                    dstReceived = dftReceived["amount"]
                    receivedAmount = self.AmountSum(dstReceived)
                except:
                    print ("Exception transfer to, line 258")
                    receivedAmount = 0
                try:
                    dftSent = dft.loc[dft["from"]==username]
                    dstSent = dftSent["amount"]
                    sentAmount = self.AmountSum(dstSent)
                except:
                    print ("Exception transfer from line 267")
                    sentAmount = 0
                bal = receivedAmount - sentAmount
                return bal, receivedAmount, sentAmount
            except:
                print ("Exception transfer line 260")
                return 0,0,0
        else:
            return 0, 0, 0


    def BalFromTransfersBulk(self, df, username, nai):
        if "type" in df.keys():
            # dft = df.loc[df["type"]=="transfer"]
            dft = df
            if "to" in dft.columns:
                dftReceived = dft.loc[dft["to"]==username]
                self._dftReceived = dftReceived
                dstReceived = dftReceived["amount"]
                dstReceived = dstReceived.dropna()
                receivedAmount = self.AmountSum(dstReceived, nai)
            else:
                receivedAmount = 0
            if "from" in dft.columns:
                dftSent = dft.loc[dft["from"]==username]
                self._dftSent = dftSent
                dstSent = dftSent["amount"]
                dstSent = dstSent.dropna()
                sentAmount = self.AmountSum(dstSent, nai)
            else:
                sentAmount = 0
            bal = receivedAmount - sentAmount
            print("From Transfers Bulk:", bal, receivedAmount, sentAmount)
            return bal, receivedAmount, sentAmount
        else:
            return 0, 0, 0

    def ClaimRegister(self, username):
        data = dict()
        data["username"] = username
        data["timestamp"] = dt.now()
        c = self.colClaimRegister.find_one({"username":username})
        if type(c) != type(None):
            c.pop("_id")
            c["message"] = "Already Registered"
        else:
            self.colClaimRegister.insert_one(data)
            c = data
            c["message"] = "Claim Registered Now"

        return c

    def Bal(self, df, username):
        if "type" in df.keys():
            # dft = df.loc[df["type"]=="transfer"]
            dftReceived = df.loc[df["to"]==username]
            dstReceived = dftReceived["amount"]
            receivedAmount = self.AmountSum(dstReceived)
            dftSent = df.loc[df["from"]==username]
            dstSent = dftSent["amount"]
            sentAmount = self.AmountSum(dstSent)
            bal = receivedAmount - sentAmount
            return bal, receivedAmount, sentAmount
        else:
            return 0, 0, 0

    def ToBeClaimedReverseTemp(self, username):
        c = self.colSnapHistory.find_one({"username":username})
        # c = list(cursor)
        if type(c) != type(None):
        # if len(c) >= 1:
            c.pop("_id")
            resultSnap = c
            print("resultSnap in Db", resultSnap)
            return resultSnap
        try:
            account = beem.account.Account(username)
        except beem.account.AccountDoesNotExistsException:
            resultSnap = dict()
            resultSnap["status"] = username + " account does not exist"
            return resultSnap
        historyGen = account.history_reverse(stop=snapshot_block, use_block_num=True)
        # historyGen = account.history()
        h = list(historyGen)
        # h = self.History(username)
        df = pd.DataFrame(h)
        self._df = df
        # bal, receivedAmount, sentAmount = self.BalFromTransfers(df, username)
        # bal, receivedAmount, sentAmount = self.BalFromTransfersBulk(df, username, nai)
        # print(bal, receivedAmount, sentAmount)

        balCurrent = account.balances["total"]
        print("Hive Current:", balCurrent[0].amount)
        # bal = balCurrent[0].amount - bal
        hiveSnap = balCurrent[0].amount - self.TestHive(username=username, df=df)
        vestSnap = balCurrent[2].amount - self.TestVest(username=username, df=df)
        if hiveSnap < 0:
            hiveSnap = 0
        if vestSnap < 0:
            vestSnap = 0
        # print("to be claimed:", bal)
        resultSnap = dict()
        resultSnap["hiveCurrent"] = balCurrent[0].amount
        resultSnap["hiveSnap"] = hiveSnap
        resultSnap["vestCurrent"] = balCurrent[2].amount
        resultSnap["vestSnap"] = vestSnap
        # resultSnap["vestSnap"] = resultSnap["vestCurrent"] - self.BalanceFromHistory(h, "reward_vests")
        resultSnap["hivePowerSnap"] = self.Vest2Hp(resultSnap["vestSnap"])
        resultSnap["Larynx"] = resultSnap["hiveSnap"] + resultSnap["hivePowerSnap"]
        resultSnap["snapshotBlock"] = snapshot_block
        resultSnap["snapshotTimestamp"] = snapshot_timestamp
        resultSnap["username"] = username
        self.colSnapHistory.insert_one(resultSnap)
        print("result written to db")
        # c = self.colClaimRegister.find_one({"username":username})
        # if type(c) != type(None):
        #     c.pop("_id")
        #     resultsnap["isClaimed"] = "Yes"
        #     resultSnap["claimRegisteredTimestamp"] = c["timestamp"]
        resultSnap.pop("_id")
        return resultSnap

    def ToBeClaimed(self, username):
        """
        """
        account = beem.account.Account(username)
        h = self.History(username)
        df = pd.DataFrame(h)
        self._df = df
        bal, receivedAmount, sentAmount = self.BalFromTransfers(df, username)
        print(bal, receivedAmount, sentAmount)

        balCurrent = account.balances["total"]
        print("Hive Current:", balCurrent[0].amount)
        bal = balCurrent[0].amount - bal
        print("to be claimed:", bal)
        return bal

    def Vest2Hp(self, vest):
        headers = CaseInsensitiveDict()
        headers["Content-Type"] = "application/x-www-form-urlencoded"
        data = '{"jsonrpc":"2.0", "method":"condenser_api.get_dynamic_global_properties", "params":[], "id":1}'
        r = requests.post(urlHive, headers=headers, data=data)
        result = json.loads(r.text)
        result = result["result"]

        hivePower = float(result["total_vesting_fund_hive"].split()[0]) * vest / float(result["total_vesting_shares"].split()[0])
        # print(resp.status_code)
        return hivePower # , result

    def BlockSee(self, df, assetType):
        dfAssetType = df[assetType].dropna()
        index = dfAssetType.index[0]
        return df.loc[index].dropna()

    def TestVest(self, username, df):
        # username = "bobinson"
        # acc = beem.account.Account(username)
        # pp.pprint(acc.balances)
        # df = pd.read_pickle("df2.pkl")
        print("username:", username)

        nai = "@@000000037"
        vestsFromTransfers, receivedAmount, sentAmount = self.BalFromTransfersBulk(df, username, nai)
        # print("vests from account transfers:", vestsFromTransfers)

        vestsDelegation = self.BalanceFromHistory(df, "delegation", nai)
        # print("vests from delegation:", vestsDelegation)

        vestsVestingPayout = self.BalanceFromHistory(df, "vesting_payout", nai)
        # print("vests from vesting_payout:", vestsVestingPayout)

        vestsVestingShares = self.BalanceFromHistory(df, "vesting_shares", nai)
        # print("vests from vesting_shares:", vestsVestingShares)

        vestsAmountIn = self.BalanceFromHistory(df, "amount_in", nai)
        # print("vests from amount_in:", vestsAmountIn)

        vestsAmountOut = self.BalanceFromHistory(df, "amount_out", nai)
        # print("vests from amount_out:", vestsAmountOut)

        vestsPayout = self.BalanceFromHistory(df, "payout", nai)
        # print("vests from payout:", vestsPayout)

        vestsInitialVestingShares = self.BalanceFromHistory(df, "initial_vesting_shares", nai)
        # print("vests from InitialVestingShares:", vestsInitialVestingShares)

        vestsInitialDelegation = self.BalanceFromHistory(df, "initial_delegation", nai)
        # print("vests from InitialDelegation :", vestsInitialDelegation)

        vestsWithdrawn = self.BalanceFromHistory(df, "withdrawn", nai)
        # print("vests from Withdrawn:", vestsWithdrawn)

        vestsDeposited = self.BalanceFromHistory(df, "deposited", nai)
        # print("vests from Deposited:", vestsDeposited)

        vestsReward = self.BalanceFromHistory(df, "reward", nai)
        # print("vests from Reward:", vestsReward)

        vestsRewardVests = self.BalanceFromHistory(df, "reward_vests", nai)
        # print("vests from RewardVests:", vestsRewardVests)

        vestsCuratorsVestingPayout = self.BalanceFromHistory(df, "curators_vesting_payout", nai)
        # print("vests from CuratorsVestingPayout:", vestsCuratorsVestingPayout)

        vestsVestingSharesReceived = self.BalanceFromHistory(df, "vesting_shares_received", nai)
        # print("vests from VestingSharesReceived:", vestsVestingSharesReceived)

        # covered by vestsVestingSharesReceived
        # powerUPVest = self.GetOptsFromHistory(df, "transfer_to_vesting_completed", "vesting_shares_received", nai)
        # print("vests from PowerUps:", powerUPVest)

        # covered by vestsReward
        # curationRewards = self.GetOptsFromHistory(df, "curation_reward", "reward", nai)
        # print("Curation Rewards", curationRewards)

        #covered by vestsVestingPayout
        # authorReward = self.GetOptsFromHistory(df, "author_reward", "vesting_payout", nai)
        # print("Author Rewards", authorReward)

        # covered by vestsWithdrawn vestsVestingPayout
        # withdrawnPowerUp = self.GetOptsFromHistory(df, "fill_vesting_withdraw", "withdrawn", nai)

        vestsTotal = vestsFromTransfers \
            + vestsVestingPayout \
            + vestsVestingShares \
            + vestsAmountIn \
            - vestsAmountOut \
            + vestsPayout \
            + vestsInitialVestingShares \
            - vestsWithdrawn \
            + vestsDeposited \
            + vestsReward \
            + vestsRewardVests \
            + vestsCuratorsVestingPayout \
            + vestsVestingSharesReceived 
            # + authorReward  
            # + powerUPVest \
            # + curationRewards \  
            # - withdrawnPowerUp

        print("vestsTotal:", vestsTotal)
        return vestsTotal


    def TestHive(self, username, df):
        # if type(df) == type(None):

        # username = "bobinson"
        # acc = beem.account.Account(username)
        # pp.pprint(acc.balances)
        # df = pd.read_pickle("df2.pkl")
        # balance from transfers

        nai = '@@000000021'
        balFromTransfers, receivedAmount, sentAmount = self.BalFromTransfersBulk(df, username, '@@000000021')
        # print("bal from account transfers", balFromTransfers)

        balHivePayout = self.BalanceFromHistory(df, "hive_payout", nai)
        # print("bal from hive_payout:", balHivePayout)

        balDeposited = self.BalanceFromHistory(df, "deposited", nai)
        # print("bal from deposited:", balDeposited)

        balRewardHive = self.BalanceFromHistory(df, "reward_hive", nai)
        # print("bal from reward_hive:", balRewardHive)

        balInterest = self.BalanceFromHistory(df, "interest", nai)
        # print("bal from interest:", balInterest)

        balAmountIn = self.BalanceFromHistory(df, "amount_in", nai)
        # print("bal from amount_in:", balAmountIn)

        # balCurrentPays = self.BalanceFromHistory(df, "current_pays", nai)
        # print("bal from current_pays:", balCurrentPays)

        # drain hive
        balHiveVested = self.BalanceFromHistory(df, "hive_vested", nai)
        # print("bal from hive_vested:", balHiveVested)

        balFee = self.BalanceFromHistory(df, "fee", nai)
        # print("bal from fee:", balFee)

        balAmountOut = self.BalanceFromHistory(df, "amount_out", nai)
        # print("bal from amount_out:", balFee)

        # covered by balHiveVested
        # spentOnPowerUP = self.GetOptsFromHistory(df, "transfer_to_vesting_completed", "hive_vested", nai)
        # print("sent to powerups:", spentOnPowerUP)

        authorReward = self.GetOptsFromHistory(df, "author_reward", "hive_payout", nai)
        # print("Author Rewards", authorReward)

        marketOrders = self.GetMarketOrders(df, "fill_order", username, nai)

        # covered by deposited
        # withdrawnPowerUp = self.GetOptsFromHistory(df, "fill_vesting_withdraw", "deposited", nai)

        # balOpenPays = self.BalanceFromHistory(df, "open_pays", nai)
        # print("bal from open_pays:", balOpenPays)
        # ---------------------------------------------------

        balTotal = balFromTransfers \
            + balHivePayout \
            + balDeposited \
            + balRewardHive \
            + balInterest \
            + balAmountIn \
            - balFee \
            - balHiveVested \
            - balAmountOut \
            + authorReward  \
            + marketOrders 
            # - spentOnPowerUP \
            # + withdrawnPowerUp \
            # - balOpenPays \
            # + balCurrentPays 

        print("balTotal:", balTotal)
        return balTotal


if __name__ == "__main__":
    snapshot = Snapshot()
    s = snapshot
    # snapshot.LoopBlocks()
    # s.Test()
    # df = pd.read_pickle("df2.pkl")
    # s.TestVest(df=df)


